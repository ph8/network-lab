package io.phaas.server;

public enum Action {
	NONE, DISCONNECT, SHUTDOWN;
}