package io.phaas.server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

/**
 * REPL for a single client connection.
 *
 */
public class ClientConnectionHandler implements Runnable {

	private final int id;
	private final Socket connection;
	private final CommandHandler handler;
	private final ConnectionHandler connectionHandler;
	private final BufferedWriter out;

	public ClientConnectionHandler(int id, Socket connection, CommandHandler handler,
			ConnectionHandler connectionHandler) throws IOException {
		this.id = id;
		this.connection = connection;
		this.handler = handler;
		this.connectionHandler = connectionHandler;
		this.out = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream()));

	}

	@Override
	public void run() {
		handler.incrementClientCount();
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

			out.write(handler.greeting());
			out.newLine();
			out.flush();

			for (String input = in.readLine(); input != null; input = in.readLine()) {
				Result result = handler.process(input);

				send(result.message);

				if (result.action == Action.SHUTDOWN) {
					connectionHandler.shutdownRequested(id);
				} else if (result.action == Action.DISCONNECT) {
					break;
				}
			}

			System.out.printf("Closing client %d socket\n", id);
			connection.close();
		} catch (IOException e) {
			System.out.printf("Client %d socket error: %s%n", id, e.getMessage());
			// e.printStackTrace();
		} finally {
			handler.decrementClientCount();
			connectionHandler.clientDisconnected(id);
		}
	}

	protected synchronized void send(String message) throws IOException {
		out.write(message);
		out.newLine();
		out.flush();
	}

	public void shutdown() throws IOException {
		send("SERVER SHUTTING DOWN");
		connection.close();
	}

}
