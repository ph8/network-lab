package io.phaas.server;

/**
 * Implementation utilizing the system clock
 */
public class TimeSourceImpl implements TimeSource {
	@Override
	public long currentTimeMillis() {
		return System.currentTimeMillis();
	}
}