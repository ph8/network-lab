package io.phaas.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Create a server socket, accept connections and create ClientConnectionHandlers for each new connection.
 */
public class Server implements Runnable, ConnectionHandler {

	public static void main(String[] args) throws IOException, InterruptedException {
		new Server(new TreeMap<String, String>()).run();
	}

	private final ServerSocket socket;
	private final ExecutorService pool;
	private final CommandHandler commandHandler;
	private final ConcurrentMap<Integer, ClientConnectionHandler> clients = new ConcurrentHashMap<>();
	private final AtomicInteger nextId = new AtomicInteger(1);

	public Server(Map<String, String> settings) throws IOException {
		this(Integer.valueOf(settings.getOrDefault("port", "57001")), //
				new CommandHandler(new TimeSourceImpl(),
						settings.getOrDefault("greeting", "Welcome to the playground.")),//
				Integer.valueOf(settings.getOrDefault("active-clients", "4")));
	}

	public Server(int port, CommandHandler commandHandler, int maxActiveConnections) throws IOException {
		this.commandHandler = commandHandler;
		this.socket = new ServerSocket(port);
		this.pool = new ThreadPoolExecutor(maxActiveConnections, maxActiveConnections, 0L, TimeUnit.MILLISECONDS,
				new LinkedBlockingQueue<Runnable>(maxActiveConnections));
	}

	@Override
	public void run() {
		System.out.printf("Accepting connections at %s%n", socket.getLocalSocketAddress());
		try {
			for (;;) {
				Socket connection = socket.accept();
				onConnect(connection);
			}
		} catch (IOException e) {
			System.out.printf("Server socket error. Shutting down. %s%n", e.getMessage());
		}

		try {
			pool.awaitTermination(1, TimeUnit.MINUTES);
			System.out.println("All clients disconnected.");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void onConnect(Socket connection) throws IOException {
		int id = getClientId();
		System.out.printf("Accepted connection %d from %s%n", id, connection.getRemoteSocketAddress());

		ClientConnectionHandler handler = new ClientConnectionHandler(id, connection, this.commandHandler, this);

		for (int retryCount = 0; retryCount < 100; retryCount++) {
			try {
				// Submit will block if the client count limit has been reached
				pool.submit(handler);
				clients.put(id, handler);
				return;
			} catch (RejectedExecutionException e) {
				System.out.printf("Could not submit task for execution (%d): %s%n", retryCount, e.getMessage());
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e1) {
				}
			}
		}
		// Failed to queue client
		connection.close();
	}

	private int getClientId() {
		return nextId.getAndIncrement();
	}

	@Override
	public void clientDisconnected(int id) {
		System.out.printf("Disconnecting client %d%n", id);
		clients.remove(id);
	}

	/**
	 * Initiate the shutdown of the server thread.
	 * 
	 * Note: This thread will be called by and execute in the context of a worker thread. Thread.currentThread is NOT
	 * the server thread.
	 * 
	 * @param id
	 */
	@Override
	public synchronized void shutdownRequested(int id) {
		System.out.printf("Shutdown requested by client %d%n", id);

		pool.shutdownNow(); // prevent new jobs from being submitted and cancel existing ones
		try {
			socket.close(); // interrupt the thread accepting connections (NOT THIS THREAD)
		} catch (IOException e) {
			System.out.printf("Exception while closing server socket: %s%n", e.getMessage());
		}

		// Close all client threads, interrupting their read() methods and leading the worker threads to exist
		for (Entry<Integer, ClientConnectionHandler> entry : clients.entrySet()) {
			try {
				entry.getValue().shutdown();
			} catch (Exception e) {
				System.out.printf("Exception while closing client %d socket: %s%n", entry.getKey(), e.getMessage());
			}
		}
	}
}
