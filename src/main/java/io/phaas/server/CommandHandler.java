package io.phaas.server;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Implementation of the server behavior
 *
 */
public class CommandHandler {

	private final TimeSource timeSource;
	private final long startTime;
	private final AtomicInteger clientCount = new AtomicInteger();
	private final String greeting;

	public CommandHandler(TimeSource timeSource, String greeting) {
		this.timeSource = timeSource;
		this.greeting = greeting;
		this.startTime = timeSource.currentTimeMillis();
	}

	public Result process(String input) {
		if ("ping".equals(input)) {
			return ping();
		} else if ("clients".equals(input)) {
			return clients();
		} else if ("run-time".equals(input)) {
			return runTime();
		} else if ("time".equals(input)) {
			return time();
		} else if (input.startsWith("echo")) {
			return echo(input);
		} else if ("goodbye".equals(input)) {
			return goodbye();
		} else if ("shutdown".equals(input)) {
			return shutdown();
		} else {
			return new Result("unknown command");
		}
	}

	private Result shutdown() {
		return new Result("ack", Action.SHUTDOWN);
	}

	private Result goodbye() {
		return new Result("ack", Action.DISCONNECT);
	}

	private Result echo(String input) {
		String response;
		if (input.length() < 6) {
			response = "";
		} else {
			response = input.substring(5, input.length());
		}
		return new Result(response);
	}

	private Result time() {
		return new Result(Long.toString(timeSource.currentTimeMillis()));
	}

	private Result runTime() {
		return new Result(Long.toString(timeSource.currentTimeMillis() - startTime));
	}

	private Result clients() {
		return new Result(Integer.toString(clientCount.get()));
	}

	private Result ping() {
		return new Result("ack");
	}

	public void setClientCount(int count) {
		clientCount.set(count);
	}

	public String greeting() {
		return greeting;
	}

	public void incrementClientCount() {
		clientCount.incrementAndGet();
	}

	public void decrementClientCount() {
		clientCount.decrementAndGet();
	}
}
