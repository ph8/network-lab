package io.phaas.server;

public class Result {
	public final String message;
	public final Action action;

	public Result(String message, Action action) {
		this.message = message;
		this.action = action;
	}

	public Result(String message) {
		this(message, Action.NONE);
	}

	@Override
	public int hashCode() {
		return action.hashCode() + message.hashCode();
	}

	/*
	 * For junit assertEquals(..)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Result other = (Result) obj;
		if (action != other.action)
			return false;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!message.equals(other.message))
			return false;
		return true;
	}

}