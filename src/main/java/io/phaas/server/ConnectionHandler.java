package io.phaas.server;

/**
 * System runtime management functionality
 *
 */
public interface ConnectionHandler {

	void clientDisconnected(int id);

	void shutdownRequested(int id);
}
