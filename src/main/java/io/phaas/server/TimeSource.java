package io.phaas.server;

/**
 * Abstraction layer between system clock and business logic to facilitate testing.
 *
 */
public interface TimeSource {

	long currentTimeMillis();

}
