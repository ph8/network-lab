package io.phaas.server;

import static org.junit.Assert.*;
import org.junit.Test;

public class CommandHandlerTest {
	private static final String GREETING = "GREETING";

	private final TimeSourceStub timeSource = new TimeSourceStub(10000000l);
	private final CommandHandler handler = new CommandHandler(timeSource, GREETING);

	@Test
	public void testPing() {
		assertEquals(new Result("ack"), handler.process("ping"));
	}

	@Test
	public void testClients() {
		assertEquals(new Result("0"), handler.process("clients"));
		handler.incrementClientCount();
		handler.incrementClientCount();
		assertEquals(new Result("2"), handler.process("clients"));

		handler.decrementClientCount();
		assertEquals(new Result("1"), handler.process("clients"));

		handler.decrementClientCount();
		assertEquals(new Result("0"), handler.process("clients"));
	}

	@Test
	public void testRunTime() {
		assertEquals(new Result("0"), handler.process("run-time"));

		timeSource.setCurrentTime(10005500l);
		assertEquals(new Result("5500"), handler.process("run-time"));

		timeSource.setCurrentTime(999999999999l);
		assertEquals(new Result("999989999999"), handler.process("run-time"));
	}

	@Test
	public void testTime() {
		assertEquals(new Result("10000000"), handler.process("time"));

		timeSource.setCurrentTime(999999999999l);
		assertEquals(new Result("999999999999"), handler.process("time"));
	}

	@Test
	public void testEcho() {
		assertEquals(new Result("World"), handler.process("echo World"));
		assertEquals(new Result(""), handler.process("echo"));
		assertEquals(new Result(""), handler.process("echo "));
	}

	@Test
	public void testUnknownCommand() {
		assertEquals(new Result("unknown command"), handler.process("invalid"));
		assertEquals(new Result("unknown command"), handler.process(""));
		assertEquals(new Result("unknown command"), handler.process("*"));
		assertEquals(new Result("unknown command"), handler.process("eChoo foo"));
		assertEquals(new Result("unknown command"), handler.process("PING"));
	}

	@Test
	public void testGreeting() {
		assertEquals(GREETING, handler.greeting());
	}

	@Test
	public void testGoodbye() {
		assertEquals(new Result("ack", Action.DISCONNECT), handler.process("goodbye"));
	}

	@Test
	public void testShutdown() {
		assertEquals(new Result("ack", Action.SHUTDOWN), handler.process("shutdown"));
	}
}
