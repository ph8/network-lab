package io.phaas.server;

import static io.phaas.server.TestClient.Status.CONNECTING;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

public class TestClient implements Runnable {

	public static enum Status {
		IDLE, CONNECTING, READY, DISCONNECTED;
	}

	public final AtomicReference<Status> status = new AtomicReference<>(Status.IDLE);
	public final BlockingQueue<String> messages = new LinkedBlockingQueue<>();
	private final Socket socket;
	private final int port;
	private BufferedWriter writer;

	public TestClient(int port) throws IOException {
		this.port = port;
		this.socket = new Socket();
	}

	@Override
	public void run() {
		try {
			status.set(CONNECTING);
			socket.connect(new InetSocketAddress("localhost", port));

			BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
			for (String line = reader.readLine(); line != null; line = reader.readLine()) {
				status.set(Status.READY);
				messages.offer(line);
			}
			socket.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		status.set(Status.DISCONNECTED);
	}

	public void send(String line) throws IOException {
		writer.write(line);
		writer.newLine();
		writer.flush();
	}

	public void disconnect() {
		try {
			socket.close();
		} catch (IOException e) {
			System.out.println("Forcefully disconnected: " + e.getMessage());
		}
	}

	public String nextMessage() throws InterruptedException {
		return messages.poll(500, TimeUnit.MILLISECONDS);
	}
}
