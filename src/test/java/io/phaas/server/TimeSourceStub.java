package io.phaas.server;

/**
 * Stub implementation for testing
 */
public class TimeSourceStub implements TimeSource {

	private long currentTime;

	public TimeSourceStub(long currentTime) {
		this.currentTime = currentTime;
	}

	public void setCurrentTime(long currentTime) {
		this.currentTime = currentTime;
	}

	public long currentTimeMillis() {
		return currentTime;
	}
}