package io.phaas.server;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

public class KillServer implements Runnable {

	public static void main(String[] args) throws Exception {

		for (int i = 0; i < 10; i++) {
			new Thread(new KillServer("localhost", 57001, 250000000)).start();
		}

		while (true) {
			Thread.sleep(60000);
		}

	}

	private final int kbyte;
	private final String host;
	private final int port;

	public KillServer(String host, int port, int kbyte) {
		this.host = host;
		this.port = port;
		this.kbyte = kbyte;

	}

	public void run() {
		while (true) {
			try {
				Socket socket = new Socket();
				socket.connect(new InetSocketAddress(host, port));

				byte[] data = new byte[1024];

				OutputStream os = socket.getOutputStream();
				int total = 0;
				for (int i = 0; i < kbyte; i++) {
					os.write(data);
					os.flush();
					total += data.length;

					if (total % (1024 * 1024) == 0) {
						System.out.printf("%n[%s] Sent %,d bytes%n", Thread.currentThread().getName(), total);
						Thread.sleep(500);
					}
				}
				socket.close();
			} catch (IOException | InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
