package io.phaas.server;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import io.phaas.server.TestClient.Status;

import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

import org.junit.Test;

public class ServerTest {

	private static final int PORT = 57001;
	private static final String GREETING = "Welcome to the playground.";

	@Test
	public void testConnectAndShutDown() throws IOException, InterruptedException {
		Thread server = startServer();

		TestClient a = connect();
		TestClient b = connect();

		assertEquals(GREETING, a.nextMessage());
		assertEquals(GREETING, b.nextMessage());

		a.send("clients");
		a.send("echo test");
		assertEquals("2", a.messages.take());
		assertEquals("test", a.messages.take());

		b.send("shutdown");
		assertEquals("ack", b.messages.take());
		assertEquals("SERVER SHUTTING DOWN", b.nextMessage());
		assertEquals("SERVER SHUTTING DOWN", a.nextMessage());

		Thread.sleep(50);

		assertEquals(Status.DISCONNECTED, a.status.get());
		assertEquals(Status.DISCONNECTED, b.status.get());

		assertFalse(server.isAlive());
	}

	@Test
	public void testMaxClients() throws IOException, InterruptedException {
		@SuppressWarnings("unused")
		Thread server = startServer();

		TestClient a = connect();
		TestClient b = connect();

		Thread.sleep(100);

		assertEquals(Status.READY, a.status.get());
		assertEquals(GREETING, a.messages.take());

		assertEquals(Status.READY, b.status.get());
		assertEquals(GREETING, b.messages.take());

		TestClient c = connect();
		TestClient d = connect();

		Thread.sleep(100);

		assertEquals(Status.CONNECTING, c.status.get());
		assertEquals(Status.CONNECTING, d.status.get());

		a.send("clients");
		assertEquals("2", a.messages.take());

		a.send("goodbye");
		assertEquals("ack", a.messages.take());

		b.send("goodbye");
		assertEquals("ack", b.messages.take());

		Thread.sleep(100);

		assertEquals(Status.DISCONNECTED, a.status.get());
		assertEquals(Status.DISCONNECTED, b.status.get());
		assertEquals(Status.READY, c.status.get());
		assertEquals(Status.READY, d.status.get());

		d.send("shutdown");
	}

	private Thread startServer() throws IOException {
		Map<String, String> settings = new TreeMap<String, String>();
		settings.put("greeting", GREETING);
		settings.put("port", "" + PORT);
		settings.put("active-clients", "2");

		Thread server = new Thread(new Server(settings));
		server.start();
		return server;
	}

	private TestClient connect() throws IOException {
		TestClient client = new TestClient(PORT);
		new Thread(client).start();
		return client;
	}
}
